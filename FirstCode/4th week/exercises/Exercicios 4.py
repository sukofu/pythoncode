# -*- coding: utf-8 -*-
"""
@author: Francisco Colaço
"""
import numpy as np
# Exercise 1 - Sudoku solver
class Sudoku():
    def __init__(self, sudoku_str):
        #Take whitespaces and newlines
        lines = sudoku_str.replace(" ","").replace("\n","")
        lst = list(map(int,lines))
        #Create the sudoku as an array
        grid = np.array(lst).reshape(9,9)
        self.sudoku = grid
        self.sudoku_original = np.array(grid, copy=True)
    def __repr__(self):
        return str(self.sudoku)
    def grid_ref(self, pos):
        #function will return grid coordinates from position 0-80
        coord = (pos//9, pos%9)
        return coord
    def grid_value(self, pos):
        #returns value from position
        return self.sudoku_original[self.grid_ref(pos)]
    def grid_cell(self, pos):
        #returns grid its positioned in
        coord = self.grid_ref(pos)
        cell_ref = (coord[0]//3, coord[1]//3)
        cell = self.sudoku[((cell_ref[0])*3):((cell_ref[0])*3)+3, ((cell_ref[1])*3):((cell_ref[1])*3)+3]
        return cell               
    def grid_row(self, pos):
        # returns the row the number is
        coord = self.grid_ref(pos)
        row_ref = coord[0]
        row = self.sudoku[row_ref:row_ref+1,0:9]
        return row
    def grid_col(self, pos):
        # returns the row the number is
        coord = self.grid_ref(pos)
        col_ref = coord[1]
        col = self.sudoku[0:9,col_ref:col_ref+1]
        return col
    def check(self, matrix):
        # Returns True if the sudoku is solved and false if it has an error
        for x in range(1,10):
            #check if it's only repeats 1 time per row and column
            if np.array(np.where(matrix[:9,x-1] == x)).size > 1 and np.array(np.where(matrix[x-1,:9] == x)).size > 1:
                return False
            #Check if it has any whitespaces(0) 
            if 0 in matrix[:9,:9]:
                return False
        return True
                
    def solve(self):
        i = 0
        check = True
        while i <= 80:
            if self.grid_value(i) == 0 and check:
                #Cell is empty in original grid, can place here.
                for a in range(1,10):
                    #Checking rows, columns, and cells
                    if a not in self.grid_cell(i) and a not in self.grid_row(i) and a not in self.grid_col(i):
                        self.sudoku[self.grid_ref(i)] = a
                        i += 1
                        break
                    else:
                        if a == 9:
                            check = False
                            i -= 1
                            break
            elif self.grid_value(i) != 0 and check:
                #Cell is filled in original grid, can't place here
                i += 1
            elif self.grid_value(i) == 0 and not check:
                #Cell is empty in original grid
                if self.sudoku[self.grid_ref(i)] == 9:
                    self.sudoku[self.grid_ref(i)] = 0
                    i -= 1
                else:
                    for a in range(self.sudoku[self.grid_ref(i)]+1,10):
                        #Checking rows, columns, and cells
                        if a not in self.grid_cell(i) and a not in self.grid_row(i) and a not in self.grid_col(i):
                            self.sudoku[self.grid_ref(i)] = a
                            i += 1
                            check = True
                            break
                        else:
                            if a == 9:
                                self.sudoku[self.grid_ref(i)] = 0
                                i -= 1
                                break
            elif self.grid_value(i) != 0 and not check:
                #Cell is filled in original grid, can't place here
                i -= 1
        return self.sudoku


sudoku = Sudoku("020501090800203006030060070001000600540000019002000700090030080200804007010907060")
print(sudoku)
solved = sudoku.solve()
print(solved)
print(sudoku.check(solved)) # Returned True meaning we have the sudoku solved

# Exercise 2 - Figure 2
# a)    Global: j(j=1), 
#       Local: a, i, j(j in range=3), p
j=1
def main():
    a = 9
    if a%2 == 0:
        a = 2 # This code is never run
    else: 
        a = 3
    print(fun1(2,4))
    for i in range(3):
        for j in range(3):
            print(fun1(a,i+j))          
def fun1(a,b):
    p = 1
    for i in range(b):
        p *= a
    return p+j

main() # b) Output :17 2 4 10 4 10 28 10 28 82

# Exercise 3
# x = [int(x) for x in input("Enter multiple value(separate each value with \",\"):\n>").split(",")]
inpt = int(input("Number:\n>"))
x = [1,2,3,7,11,13]
def findMultiple(lst):
    string = "Values not found"
    for i in range(len(x)):
        for j in range(i,len(x)):
            if x[i]*x[j] == inpt:
                string = "{} and {}".format(x[i], x[j])
    return string

print(findMultiple(x))
        
    
