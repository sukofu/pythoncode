# -*- coding: utf-8 -*-
"""
@author: Francisco Colaço
"""
import numpy as np

m = np.array([
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 9]])

# Exercise 1
print("Exercise 1:")
print(m[0, :])

# Exercise 2
print("\nExercise 2:")
print(m[:, 0])

# Exercise 3
print("\nExercise 3:")
for i in range(3):
    print(m[i, i])

# Exercice 4
print("\nExercise 4:")
for i in range(3):
    print(m[i, i-2*i-1])

# Exercise 5
print("\nExercise 5:")
print(np.sum(m, axis=1))

# Exercise 6
print("\nExercise 5:")
print(np.sum(m, axis=0))
