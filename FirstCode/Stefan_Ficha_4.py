# -*- coding: utf-8 -*-
"""
@author: Francisco Colaço
"""
# Exercicio 1
# out_file = open("inFile.txt", 'w')
# for i in range(100):
#   out_file.write(str(i)) # Escreve os número de 1 a 100 no ficheiro
 
# in_file = open("inFile.txt",'r')
# indata = in_file.read()
# in_file.close()
# indata = in_file.read() 
# in_file.close()  # Erro de tentar abrir um ficheiro fechado

# in_file = open(“inFile.txt”,'r')
# print(in_file.readline())
# in_file = open(“inFile.txt” ,'r'))
# print(in_file.readline())
# in_file.close() # O ficheiro já está aberto

# Exercicio 2 e 3
# with open("test.txt") as file_object:
#     lines = file_object.readlines()
#     for i in range(len(lines)):
#         print("line #{} - {}".format(i+1,lines[i]))
        
# Exercicio 4 e 5
# import csv
# def media(file):
#     try: 
#         if not open(file):
#             raise IOError
#         with open(file, "r") as file_object:
#             reader = csv.reader(file_object, delimiter=' ')
#             lst = list(reader)
#             total = 0
#             for j in lst:
#                 total_row = 0
#                 for i in j:
#                     total_row += float(i)
#                 print("Day {} mean : {:.2f}".format(lst.index(j)+1,total_row/len(j)))
#                 print(total_row)
#                 total += total_row/len(j)
#             print("Total mean: {:.2f}".format(total/len(lst)))
#     except IOError as e:
#         print(e.__class__, "Try to use a valid file")
# media("temps.csv")

# Exercicio 6
# def linha_para_elemento(file_name):
#     atoms = {"nome": [], "atomico": [],"densidade": []}
#     try: 
#         if not open(file_name):
#             raise IOError
#         with open(file_name, "r") as file_object:
#             for line in file_object:
#                 (a,b,c) = line.split()
#                 atoms["nome"].append(str(a))
#                 atoms["atomico"].append(int(b))
#                 atoms["densidade"].append(float(c))
#         print(atoms)
#     except IOError as e:
#         print(e.__class__, "Try to use a valid file")
# linha_para_elemento("atoms.txt")

# Exercicio 7

# Exercicio 8
            
    
