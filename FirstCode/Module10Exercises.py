# -*- coding: utf-8 -*-
"""
@author: Francisco Colaço
"""
# Exercise 1
x = (4, 5)
def add_tuple(tup, value):
    if type(tup) is tuple:
        lst = list(tup)
        lst.append(value)
        tupl = tuple(lst)
    else:
        return "The input is not tuple"
    return tupl
print(add_tuple(x,6))

# Exercise 2
message = "$ $ Python$#Course $"
message = message.replace("$", "")
message = message.replace("#", " ")
message = message.strip()
print(message)

# Exercise 3
A = [1, 2, "A"]
B = ("Python", 161.8, 0, 5)
C = {10, 12, 14, 16, 18, 20}

min_ABC = min(len(A), len(B), len(C))
C = list(C)
D = []
for i in range(min_ABC):
    my_tuple = (A[i], B[i], C[i])
    D.append(my_tuple)
print(D)

# Exercise 4
num = [8, 2, (9, 3), 4, (1, 6, 7), 34]
count = 0
for i in num:
    if type(i) is tuple:
        count += 1
print("There are {} tuples in \"num\"".format(count))