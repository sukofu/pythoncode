# -*- coding: utf-8 -*-
"""
@author: Francisco Colaço
"""
# Hangman game
import random
# words to guess - months
words = ["january", "february", "march", "april", "may", "june", "july", \
         "august", "september", "october", "november", "december"]
    
def play_loop():
    # A loop to re-execute the game when the round ends:
    play_game = input("Do you want to play again? y = yes, n = no \n")
    while play_game not in ["y", "n", "Y", "N"]:
        play_game = input("Do you want to play again? y = yes, n = no \n")
    if play_game.lower() == "y":
        main()
    elif play_game.lower() == "n":
        print("Thanks for playing!")
        return "n"
def main():
    global count
    global word
    global display
    global limit
    global victory
    count = 0
    word = random.choice(words)
    lenght = len(word)
    display = "_" * lenght
    limit = 5
    victory = False
    hangman()
def hangman():
    # Initializing all the conditions requires to play
    global word
    global victory
    global display
    global limit
    global count
    word_original = word
    # The game loop
    while count < limit and victory == False:
        guess = input("This is the Hangman world: {0}\nEnter your guess:\n>".format(display))
        guess = guess.strip().lower()
        if len(guess) == 0 or len(guess) >= 2 or not guess.isalpha():
            print("Invalid input! Please input a letter\n")
            hangman()
        elif guess in word:
            index = word.find(guess)
            word = word.replace(guess, "_", 1)
            display = display[:index] + guess + display[index+1:]
        else: # Failed conditions
            count += 1
            if count == 1:
                print("   _____ \n"
                      "  |      \n"
                      "  |      \n"
                      "  |      \n"
                      "  |      \n"
                      "  |      \n"
                      "  |      \n"
                      "__|__\n")
                print("Wrong guess. " + str(limit - count) + " guesses remaining\n")
            elif count == 2:
                print("   _____ \n"
                      "  |     | \n"
                      "  |     |\n"
                      "  |      \n"
                      "  |      \n"
                      "  |      \n"
                      "  |      \n"
                      "__|__\n")
                print("Wrong guess. " + str(limit - count) + " guesses remaining\n")
            elif count == 3:
               print("   _____ \n"
                     "  |     | \n"
                     "  |     |\n"
                     "  |     | \n"
                     "  |      \n"
                     "  |      \n"
                     "  |      \n"
                     "__|__\n")
               print("Wrong guess. " + str(limit - count) + " guesses remaining\n")
            elif count == 4:
                print("   _____ \n"
                      "  |     | \n"
                      "  |     |\n"
                      "  |     | \n"
                      "  |     O \n"
                      "  |      \n"
                      "  |      \n"
                      "__|__\n")
                print("Wrong guess. " + str(limit - count) + " last guess remaining\n")
            elif count == 5:
                print("   _____ \n"
                  "  |     | \n"
                  "  |     |\n"
                  "  |     | \n"
                  "  |     O \n"
                  "  |    /|\ \n"
                  "  |    / \ \n"
                  "__|__\n")
                print("Wrong guess. You are hanged!!!\n")
                print("The word was:",word_original)
        if word == "_"*len(word):
            print("Congratulations! You have guessed the word:",display)
            victory = True
    play_loop()

#Greeting
print("\nWelcome to Hangman by Francisco Colaço" \
      "\nHere you have to guess the name of the month before you hang")
player = str(input("Enter your name:\n>"))
print("Hello {0}! Best of luck!".format(player))
#Game start
main()