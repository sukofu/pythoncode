# -*- coding: utf-8 -*-
"""
@author: Francisco Colaço
"""
#Weakness Table:
normal = {"Fighting": 2,"Ghost": 0}
fighting = {"Flying": 2, "Rock": 0.5, "Bug": 0.5, "Dark": 0.5, "Fairy": 2}
flying = {"Fighting": 0.5, "Ground": 0, "Rock": 2, "Bug": 0.5, "Grass": 0.5, "Electric": 2, "Ice": 2}
poison = {"Fighting": 0.5, "Poison": 0.5, "Ground": 2, "Bug": 0.5, "Grass": 0.5, "Psychic": 2, "Fairy": 0.5}
ground = {"Poison": 0.5, "Rock": 0.5, "Water": 2, "Grass": 2, "Electric": 0, "Ice": 2}
rock = {"Normal": 0.5, "Fighting": 2, "Flying": 0.5, "Poison": 0.5, "Ground": 2, "Steel": 2, "Fire": 0.5, "Water": 2, "Grass": 0.5}
bug = {"Fighting": 0.5, "Flying": 2, "Ground": 0.5, "Rock": 2, "Fire": 2, "Grass": 0.5}
ghost = {"Normal": 0, "Fighting": 0, "Poison": 0.5, "Bug": 0.5, "Ghost": 2, "Dark": 2}
steel = {"Normal": 0.5, "Fighting": 2, "Flying": 0.5, "Poison": 0, "Ground": 2, "Rock": 0.5, "Bug": 0.5, "Steel": 0.5, "Fire": 2, "Grass": 0.5, "Psychic": 0.5, "Ice": 0.5, "Dragon": 0.5, "Fairy": 0.5}
fire = {"Ground": 2, "Rock": 2, "Bug": 0.5, "Steel": 0.5, "Fire": 0.5, "Water": 2, "Grass": 0.5, "Ice": 0.5, "Fairy": 0.5}
water ={"Steel": 0.5, "Fire": 0.5, "Water": 0.5, "Grass": 2, "Electric": 2, "Ice": 0.5}
grass = {"Flying": 2, "Poison": 2, "Ground": 0.5, "Bug": 2, "Fire": 2, "Water": 0.5, "Grass": 0.5, "Electric": 0.5, "Ice": 2}
electric = {"Flying": 0.5, "Ground": 2, "Steel": 0.5, "Electric": 0.5}
psychic = {"Fighting": 0.5, "Bug": 2, "Ghost": 2, "Psychic": 0.5, "Dark": 2}
ice = {"Fighting": 2, "Rock": 2, "Steel": 2, "Fire": 2, "Ice": 0.5}
dragon ={"Fire": 0.5, "Water": 0.5, "Grass": 0.5, "Electric": 0.5, "Ice": 2, "Dragon": 2, "Fairy": 2}
dark = {"Fighting": 2, "Bug": 2, "Ghost": 0.5, "Psychic": 0, "Dark": 0.5, "Fairy": 2}
fairy = {"Fighting": 0.5, "Poison": 2, "Bug": 0.5, "Steel": 2, "Dragon": 0, "Dark": 0.5}
types = {"Normal": normal, "Fighting": fighting, "Flying": flying, "Poison": poison, "Ground": ground, "Rock": rock, "Bug": bug, "Ghost": ghost, "Steel": steel, "Fire": fire, "Water": water, "Grass": grass, "Electric": electric, "Psychic": psychic, "Ice": ice, "Dragon": dragon, "Dark": dark, "Fairy": fairy}

#Pokemon Class
class Pokemon:
    def __init__(self, name, level, type, max_health, current_health, ko=False):
        self.name = name
        self.level = level
        self.type = type
        self.max_health = max_health
        self.current_health = current_health
        self.ko = ko  
    def __repr__(self):
        return str("{0}\nHealth: {1}/{2}\nAttack: {3}".format(self.name, self.current_health, self.max_health, self.level))
    def gain_health(self):
        potion = 50
        while self.current_health < self.max_health:
            self.current_health += 1
            potion -= 1
            if potion == 0:
                self.ko = False
                break
        print("{0} now has {1} health".format(self.name, self.current_health))
    def lose_health(self, value):
        damage = value
        if self.ko:
            print("{0} is Knocked Out. Can't lose health!".format(self.name))
        else:
            while True:
                self.current_health -= 1
                damage -= 1
                if self.current_health <= 0:
                    self.knocked_out()
                    return
                if damage == 0:
                    break
            print("{0} now has {1} health".format(self.name, self.current_health)) 
    def knocked_out(self):
        self.ko = True
        print("{0} is out!".format(self.name))
    def attack(self, other_pokemon):
        if other_pokemon.ko:
            print("{0} is Knocked Out. Can't attack!".format(other_pokemon.name))
        else:
            type_mult = types[str(self.type)]
            mult = type_mult.get(other_pokemon.type, 1)
            damage = self.level / mult 
            print("{0} did {1} damage to {2}".format(self.name, damage, other_pokemon.name))
            other_pokemon.lose_health(int(damage))
            
#Trainer Class
class Trainer:
    def __init__(self, name, pokemons=[], potions=0, current_pokemon=0):
        self.name = name
        self.pokemons = pokemons
        self.potions = potions
        self.current_pokemon = 0
    def __repr__(self):
        poke = [p.name for p in self.pokemons]
        return "Name: {0}\nPokemons: {1}\nPotions: {2}\nActive Pokemon: {3}".format(self.name, poke, self.potions, self.pokemons[self.current_pokemon].name)
    def use_potion(self):
        injured_pokemon = self.pokemons[self.current_pokemon]
        if injured_pokemon.current_health < injured_pokemon.max_health:
            injured_pokemon.gain_health()
            self.potions -= 1
        else: print("{0} is already fully healed!".format(injured_pokemon.name))
    def attack(self, trainer):
        self.pokemons[self.current_pokemon].attack(trainer.pokemons[trainer.current_pokemon])
        if trainer.pokemons[trainer.current_pokemon].ko == True:
            poke = [p.name for p in self.pokemons if p.ko == False]
            i = input("Which pokemon do you want to switch?\n{0}".format(poke))
            trainer.switch_pokemon(i)
    def switch_pokemon(self, pokemon):
        for i in range(len(self.pokemons)):
            if pokemon == self.pokemons[i].name:
                if self.pokemons[i].ko == True:
                    print("Can't switch to knocked out pokemon!")
                else: 
                    self.current_pokemon = i
                    return
        print("Can't find {0} in {1}'s team!".format(pokemon, self.name))

# Objects
charmander = Pokemon("Charmander", 10, "Fire", 100, 100)
squirtle = Pokemon("Squirtle", 10, "Water", 110, 110)
bulbasaur = Pokemon("Bulbasaur", 10, "Grass", 120, 120)
pidgey = Pokemon("Pidgey", 10, "Flying", 80, 80)

ash = Trainer("Ash", [charmander, bulbasaur], 10)
gary = Trainer("Gary", [squirtle, pidgey], 10)

#Driver
gary.switch_pokemon("Pidgey")
ash.attack(gary)

