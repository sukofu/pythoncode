# -*- coding: utf-8 -*-
"""
@author: Francisco Colaço
"""
import datetime
from datetime import timedelta
from decimal import Decimal
# Exercicio 1
# Escreva em linguagem python um programa que leia uma temperatura em graus farenheit e a converta em celcius
def f_to_c(f_temp):
  c_temp = (f_temp - 32) * 5/9
  return c_temp

def c_to_f(c_temp):
  f_temp = c_temp * (9/5) + 32
  return f_temp

# Exercicio 2
# Escreva que pegue o ano de nascimento da pessoa e imprime a idade que a pessoa vai ter ao fim do ano
x = datetime.datetime.now()
def age(year):
    return x.year-year
year = int(input("Whats your birth year?"))
print("So you are",age(year),"years old")

# Exercicio 3
# Escreva um programa que leia uma hora em horas, minutos e segundos e os traduza para segundos
def time_to_seconds(hour, minutes, seconds):
    return seconds + hour*3600 + minutes*60
print("It's",time_to_seconds(x.hour, x.minute, x.second),"seconds past midnight")

# Exercicio 5
# A) output: z = 1
z = 1
if z == 1:
    z += 1 # z=2
    if z == 1:
        z += 1
    else:
        z -= 1 # z=1
else:
    z -= 1
    
#B) output: z = -3
z = -1
if z == 1:
    z += 1 
    if z == 1:
        z += 1
    else:
        z -= 1 # z=-2
else:
    z -= 1 # z=-3
    
#C) O segundo if block nunca vai ser executado porque existem dois "nested ifs" com a mesma condição 
if z == 1:
    z += 1 
    if z == 1: ##
        z += 1 ##
    else:
        z -= 1 
else:
    z -= 1 
# Exercicio 6
# Escreva em linguagem Python um programa que leia um número inteiro positivo e escreva no ecrã a sua representação em numeração romana
def int_to_roman(num):
    result = ""
    roman = {1000: "M", 900: "CM", 500: "D", 400: "CD", 100: "C", 90: "XC", 50: "L", 40: "XL", 10: "X", 9: "IX", 5: "V", 4: "IV", 1: "I"}
    
    while num != 0:
        for i, j in roman.items():
            if num >= i:
                dividend = num//i
                num %= i
                result += dividend*j
    return result
print(int_to_roman(49))

# Exercicio 7
# Escreva em linguagem python um programa que leia um ano (>0) e escreva o século a que pertence
ano = int(input("Escreva um ano\n>"))
if ano%100 == 0:
    sec = ano // 100
else:
    sec = ano // 100 +1
print(f"O ano de {ano} pertence ao século {int_to_roman(sec)}.")

# Exercicio 8
# Escreva um programa que receba ano, mês e dia em separado e um numero de dias X e devolva a data X dias mais tarde
def days_later_datetime(y, m, d, later):
    x = datetime.date(y, m, d)
    x += datetime.timedelta(later)
    return x

def days_later(y, m, d, later):
    total_days = d+ later
    if y % 4 == 0 and  m == 2:
        if total_days >29:
            m += 1
            d += later - 29
        else:
            d += later
    else:
        if m == 12 and total_days > 31:
            y += 1
            m += total_days // 31 -12
            d += later - (31*(total_days // 31))
        elif m == 2:
            if total_days >28:
                m += 1
                d = (total_days) - 28
            else:
                d += later
        elif m in [1, 3, 5, 7, 8, 10, 12]:
            if total_days > 31:
                m += 1
                d += later - 31
            else:
                d += later
        else:
            if total_days > 30:
                m += 1
                d += later - 30
            else:
                d += later
    return "The new date is {day}-{month}-{year}".format(day=d, month=m, year=y)
            
print(days_later_datetime(2019, 12, 29, 60))
print(days_later(2019, 12, 29, 60))

# Exercicio 9
# Indique os erros sintáticos no seguinte programa em Python:
x = 1
y = 1
while x == 1 and y < 5: # Em vez de compararmos um condição estavamos a declarar "x" e falatava um ":" para o loop funcionar corretamente
    y = y + 2
    
# Exercicio 10
# Este programa em Python tem como objectivo escrever a tabuada do número inteiro dado pelo utilizador. Explique porque é que este programa não termina, corrija o erro e especifique qual o valor das variáveis n e i no final da execução do programa corrigido
n = int(input("Escreve um número inteiro: "))
print("Tabuada do", n, ":")
i = 1
while i <= 10:
    print(n, "x", i, "=", n * i)
    i += 1 # não estavamos a fazer nada ao i
    
# Exercicio 11
# Considere este programa em Python
dividendo = int(input("Dividendo: "))
divisor = int(input("Divisor: "))
resto = dividendo
quociente = 0
while resto >= divisor:
    resto = resto - divisor
    quociente = quociente + 1
    print("O quociente é", quociente, "e o resto é", resto)

#A) O programa traduz o que seria uma divisão feita à mão
#B) Para o resto existe "%" e para a divisão temos "/"
#C)
dividendo = int(input("Dividendo: "))
divisor = int(input("Divisor: "))
while dividendo < divisor:
    print("\nPara uma divisão inteira o dividendo tem de ser maior que o divisor")
    dividendo = int(input("Dividendo: "))
    divisor = int(input("Divisor: "))
resto = dividendo
quociente = 0
while resto >= divisor:
    resto = resto - divisor
    quociente = quociente + 1
    print("O quociente é", quociente, "e o resto é", resto)
#D)
dividendo = int(input("Dividendo: "))
divisor = int(input("Divisor: "))
while dividendo < divisor or dividendo < 1 or divisor < 1:
    print("\nPara uma divisão positiva inteira um dividendo positivo tem de ser maior que o divisor positivo")
    dividendo = int(input("Dividendo: "))
    divisor = int(input("Divisor: "))
resto = dividendo
quociente = 0
while resto >= divisor:
    resto = resto - divisor
    quociente = quociente + 1
    print("O quociente é", quociente, "e o resto é", resto)

# Exercicio 12
while True:
    n = int(input("Escreve um número inteiro inferior a 1000: "))
    if n <= 1000:
        break
current_n = 1
while current_n <= n:
    i = current_n
    f = 1
    print(i)
    while i > 1:
        f = f * i
        i = i - 1
        print("Factorial de " + str(current_n) + ": " + str(f))
    current_n = current_n + 1

# Exercicio 13
while True:
    n = float(input("Introduza um número decimal e eu retornarei só a parte positiva:\n>"))
    print("Número devolvido: {}".format(int(n - (n%1))))
    choice = input("\n Programa concluido, precisa de introduzir mais resultados?\nn para não\n>")
    if choice == "n":
        break

# Exercicio 14
string = str(input("Introduza uma frase:\n>"))
num = int(input("Introduza o numero de vezes que que ver a frase escolhida:\n>"))
for i in range(num):
    print("{} - {}".format(i+1, string))

# Exercicio 15
num = int(input("Introduza um numero\n>"))
for i in range(num+1):
    print("3**{} = {}".format(i, 3**i))

# Exercicio 16
num = int(input("Introduza um numero\n>"))
res = 0
k = 2
while res < num:
    ls=[]
    for i in range(1,k):
        if k%i == 0:
            ls.append(i)
    if len(ls) == 1:
        res = 2**(k-1)*(2**k-1)
        if res > num:
            break
        print("Numero perfeito: {}".format(res))
    k += 1

# Exercicio 17
num = int(input("Introduza um numero maior que 10\n>"))
count = 0
for i in range(10, num+1):
    num_s = str(i)
    if num_s[0:] == num_s[::-1]:
        count += 1
print("Entre 10 e", num,"existem", count,"capicuas")