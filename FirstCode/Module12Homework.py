# -*- coding: utf-8 -*-
"""
@author: Francisco Colaço
"""
# Exercise 1
d1 = {"a":1, "b":3, "c":2}
d2 = {"a":2, "b":3, "c":1}
w_d1 = set(d1.items())
w_d2 = set(d2.items())
print(dict(w_d1.intersection(w_d2)))

# Exercise 2
#No intersection
#a <= X < d
#No intersection
#c <= X <= b