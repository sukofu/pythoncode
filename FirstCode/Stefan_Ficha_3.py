# -*- coding: utf-8 -*-
"""
@author: Francisco Colaço
"""
# 1. Qual o valor de cada expressão?
#a. map(lambda x:x+1, range(1,4))
# [2, 3, 4]

#b. map(lambda x:x>0, [3,−5,−2,0])
#[True, False, False, False]

#c. filter(lambda x:x>5, range(1,7))
#[6]

#d. filter(lambda x:x%2==0, range(1,11))
#[2,4,6,8,10]

#2. Determine o valor de cada uma das expressões seguintes:
#a. reduce(lambda y, z: y* 3+z, range(1,5))
# (((1*3+2)*3+3)*3+4) = 58

#b. reduce (lambda x, y: x** 2+y, range(2,6))
# (((2**2+3)**2+4)**2+5) = 2814