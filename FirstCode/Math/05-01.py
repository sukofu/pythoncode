# -*- coding: utf-8 -*-
"""
@author: Francisco Colaço
@date: 05/01/2021
"""

from matplotlib import pyplot as plt
import numpy as np
fig = plt.figure()

#Funtions
#Exercise 1
y = (3**2)/3
plt.axhline(y, label='y = (3**2)/3')
plt.ylabel('Functions - Exercise 1')
plt.legend()
plt.show() 

# Exercise 2
y = (3**4)/3
plt.axhline(y, label='y = (3**4)/3')
plt.ylabel('Functions - Exercise 2')
plt.legend()
plt.show() 

# Exercise 3
# m, n = 
# y = (3*n*m**2)/3*n
# plt.ylabel('Functions - Exercise 3')
# plt.plot(y, label='y = (3*n*m**2)/3*n')
# plt.legend()
# plt.show() 

#Logarithms
#Exercise 1 --- 2 = log7 * x
y = np.linspace(-10,50)
x = y + 7**2
plt.plot(x, y)
plt.ylabel('Logarithms - Exercise 1')
plt.legend()
plt.show() 

#Exercise 2 --- 3 = log10 * (x+8)
x = (10**3) - 8
plt.axhline(x, label='x = (10**3) - 8')
plt.ylabel('Logarithms - Exercise 2')
plt.legend()
plt.show() 


#Exercise 3 --- log5 * 125 = x
x = np.log(5) * 125
plt.axvline(x, label='x = log5 * 125')
plt.ylabel('Logarithms - Exercise 3')
plt.legend()
plt.show() 

