# -*- coding: utf-8 -*-
"""
@aauthor: francisco Colaço
"""
# Exercise 1
my_list = [1, 2, 23, 4, "word"]
for i in [0, 1, 2, 3, 4]:
    if i == len(my_list)-1:
        break
    print(my_list[i], my_list[i+1])
    
for i in range(len(my_list)-1):
    print(my_list[i], my_list[i+1])

# Exercise 2
a = [7, 5, 30, 2, 6, 25]
highest = 0
for i in a:
    if i > highest:
        highest = i
print(highest)

# Exercise 3
a = [1, 2]
b = [1, 4, 5]
c = []
for i in a:
    for j in b:
        if i != j:
            c.append((i,j))
print(c) # c = [(1, 4), (1, 5), (2, 1), (2, 4), (2, 5)]