# -*- coding: utf-8 -*-
"""
@author: Francisco Colaço
"""
# Exercise 1
A = {1, 2, 3, 4, 6, 9, 10}
B = {1, 3, 4, 9, 13, 14, 15}
C = {1, 2, 3, 6, 9, 11, 12, 14, 15}
# 2, 6, 11, 12, 14, 15
answer1 = C - (A.intersection(B))
print(answer1)
# 4
answer2 = (A.intersection(B)) - C
print(answer2)
# 4, 10, 13
answer3 = (A.union(B)) - C
print(answer3)

# Exercicio 2
letters = {"a", "y", "c", "o", "z"}
sentence = "Python Course"
sentence = set(sentence)
print(letters.intersection(sentence))