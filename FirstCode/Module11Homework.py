# -*- coding: utf-8 -*-
"""
@author: Francisco Colaço
"""
person = {"name": "John Smith", "phone":{"home": "01-4455", "mobile": "918-123456"}, "children": ["Olivia", "Sophia"], "age": 48}

print("Length:", len(person))
print("Name:", person["name"])
print("Home phone:", person["phone"]["home"])
print("Mobile phone:", person["phone"]["mobile"])
print("Children:", person["children"])
print("First Child:", person["children"][0])
print("Age:", person.pop("age"))

