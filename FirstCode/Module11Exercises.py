# -*- coding: utf-8 -*-
"""
@author: Francisco Colaço
"""
# Exercise 1
s = "abfabdcaa"
d_s = {}
for i in s:
    d_s[i] = d_s.get(i,0)+1
print(d_s)

# Exercise 2
line =  "a dictionary is a datastructure."
d_line = {}
line = line.split()
for i in line:
    d_line[i] = d_line.get(i, 0) +1
print(d_line)

# Exercise 3
lines = "a dictionary is a datastructure\n a set is also a datastructure"
def num_occurences(string):
    occurences = {}
    words = lines.split()    
    for word in words:
        count = words.count(word)
        occurences[word] = count
    return occurences
print(num_occurences(lines))

# Exercise 4
d = {"a": 4, "b":2, "f": 1, "d": 1, "e":1}
total = 0
for i in d.values():
    total += i
print(total)
# Exercise 5 
d1 = {"x":3, "y":2, "z":1}
d2 = {"w":8, "t":7, "z":5}
for i, j in d2.items():
    if i in d1:
        d1[i] += d2[i]
    else: 
        d1.update({i:j})
print(d1)