# -*- coding: utf-8 -*-
"""
@author: Francisco Colaço
"""
# # Exercicio 1
# def imprimeDivisaoInteira(x, y):
#     if y == 0:
#         return "Divisao por zero"
#     else:
#         return x//y
# #A) A função retorna uma divisao inteira de x por y se y não for 0
# #B) 
# imprimeDivisaoInteira(4, 2) #2
# imprimeDivisaoInteira(2, 4) #0
# imprimeDivisaoInteira(3, 0) #"Divisao por zero"
# help(imprimeDivisaoInteira) # imprimeDivisaoInteira(x, y)
# imprimeDivisaoInteira() # Falta 2 argumentos requeridos
# #C)
# def imprimeDivisaoInteira(x, y):
#     """
#     A função retorna uma divisao inteira de x por y se y não for 0
#     """
#     if y == 0:
#         return "Divisao por zero"
#     else:
#         return x//y

# # Exercicio 2
# #c)
# def potencia(a, b=None):
#     if b is None:
#         b = a 
#     return a**b

# #a)
# a = 2
# b = 3
# potencia(b, a) # 3**2 = 9
# potencia(a, b) #2**3 = 8
# print(potencia(2, 0)) # 2**0 = 1
# print(potencia(2)) # TypeError, there are two required parameters

# #b)
# def potenciaP(a):
#     return a**a

# # Exercicio 3
# a = 4
# def printFuncao():
#     a = 17
#     print("Dentro da funcao:", a)
# printFuncao() #A) Vai retornar "Dentro da funcao: 17"
# print("Fora da funcao:", a)
# #B) Uma variavel global é uma variavel que pode ser lida por qualquer função, uma variavel local é uma variavle que só pode ser lida dentro da função

# # Exercicio 4
# def somaDivisores(num):
#     """
#     Soma de divisores de um numero dado
#     Requires:
#     num seja int e num > 0
#     Ensures: um int correspondente à soma dos divisores
#     de num que sejam maiores que 1 e menores que num
#     """
# # A)
#     total = 0
#     ls = []
#     for i in range(1,num):
#         if num%i == 0:
#             ls.append(i)
#     for i in ls:
#         total += i 
#     return total
# # B) um int correspondente à soma dos divisores de "num" que sejam maiores que 1 e menores que "num"

# # Exercicio 5
# while True:
#     num = int(input("Introduza um número positivo:\n>"))
#     if num < 0:
#         break
#     print("Soma dos divisores de", num, ":", somaDivisores(num))

# # Exercicio 6
# def dados():
#     dia = int(input("Introduza o ano de nascimento:\n>"))
#     mes = int(input("Introduza o mês de nascimento:\n>"))
#     ano = int(input("Introduza o dia de nascimento:\n>"))
#     return (dia, mes, ano)

# def anos(data, today=(2015, 10, 2)):
#     if data[0] > today[0] or (data[0] and data[2] > today[2]):
#         print("Pai tem", today[0]-data[0]-1,"anos")
#     else:
#         print("Pai tem", today[0]-data[0],"anos")
    
# print("\nDados do Pai")
# pai = dados()
# anos(pai)
# print("\nDados do Mae")
# mae = dados()
# anos(mae)

# # Exercicio 7
# def maior(a, b):
#     """

#     Parameters
#     ----------
#     a : INT
#     b : INT

#     Returns
#     -------
#     TYPE
#         Returns the larger of the 2 numbers

#     """
#     return max(a,b)

# def menor(a, b):
#     if maior(a, b) == a:
#         return b
#     else: return a
    
# # Exercicio 8
# def take_unit_part(n):
#     """

#     Parameters
#     ----------
#     n : str
#         A number trapped in a string

#     Returns
#     -------
#     INT
#     Takes the unit from the number 

#     """
#     return "Numero devolvido: {}".format(int(n[:-1]))
# n = str(input("Escreva um numero que queira a casa das unidades retirada:\n>"))

# # print(take_unit_part(n))

# # Exercicio 9
# def adds_0(num):
#     """
#     Parameters
#     ----------
#     num : int
#         A normal int

#     Returns
#     -------
#     "num" with 0 in the end

#     """
#     return num*10

# print(adds_0(0)) # 0
# print(adds_0(73)) # 730
# numb = int(input("Escreva um numero:\n>"))
# print(adds_0(numb))

# Exercicio 10
def somaDivisores(num):
    """
    Soma de divisores próprios de um numero dado
    Requires:
    num seja int e num > 0
    Ensures: um int correspondente à soma dos divisores
    de num que sejam maiores que 1 e menores que num
    """
    total = 0
    ls = []
    for i in range(1,num):
        if num%i == 0:
            ls.append(i)
    for i in ls:
        total += i 
    return total

# Exercicio 11
def check_if_prime(num):
    """
    Parameters
    ----------
    num : int
        To check if it's prime or not

    Returns
    -------
    True if prime, False if not

    """
    count = 0
    
    for j in range(2,num+1):
        ls= []
        for i in range(1,j+1):
            if j%i == 0:
                ls.append(i)
        if len(ls) == 2:
                count +=1  
    return "Existem {} números primos entre 2 e {}".format(count, num)
print(check_if_prime(7919))
while True:    
    numb = int(input("Escreva um numero para quantos números primos existem entre 2 e o seu número:\n>"))
    if numb > 1:
        break
    print("\nPor favor escolha um número maior que 2")
print(check_if_prime(numb))
