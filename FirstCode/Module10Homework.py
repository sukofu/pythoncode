# -*- coding: utf-8 -*-
"""
@author: Francisco Colaço
"""
# Exercise 1
a = [(1, 2, 3), (4, 5, 6)]
a1, a2 = a
a1 = list(a1)
a2 = list(a2)
a1[-1] = 9
a2[-1] = 9
a1 = tuple(a1)
a2 = tuple(a2)
b = []
b.extend((a1, a2))
print(b)

# Exercise 2
my_obj = ((1,3), (2,4))
for i, j in my_obj:
    print("i is", i)
    print("j is", j)
#The “TypeError: cannot unpack non-iterable NoneType object” error is raised when you try to unpack values that does not return a value.

# Exercise 3
inpt = [(1,3), (2,4), ("A", 8)]
tpl1, tpl2 = [], []
for i, j in inpt:
    tpl1 += [i]
    tpl2 += [j]
tpl1 = tuple(tpl1)
tpl2 = tuple(tpl2)
outpt = []
outpt.extend((tpl1,tpl2))
print(outpt)