# -*- coding: utf-8 -*-
"""
@author: Francisco Colaço
"""
# Exercise 1
def even_odd(n):
    while n != 0:
        if n%2 != 0:
            print("Odd number:",n)
        else: print("Even number:",n)
        n -= 1

even_odd(10)

