# -*- coding: utf-8 -*-
"""
@author: Francisco Colaço
"""
# Exercise 2
def mean(V):
    total = 0
    for number in V:
        total += number
    total /= len(V)
    return total

print(mean([2, 61, -1, 0, 88, 55, 3, 121, 25, 75]))

