# -*- coding: utf-8 -*-
"""
@author: Francisco Colaço
"""
#Exercise 2
#.a
age=int(input("How old are you?\nI'm "))
print(f"For {age} years old, you look great!")

#.b
if age < 16:
    print("You are too young to work, come back to school ")
elif 15 < age < 66:
    print("Have a good day at work")
else:
    print("You have worked enough, Let’s Travel now")