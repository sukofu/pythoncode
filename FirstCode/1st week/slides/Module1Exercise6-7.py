# -*- coding: utf-8 -*-
"""
@author: Franciso Colaço
"""
# Exercise 6
msg = "Lisbon is in Portugal"

print(msg[1]) #i
print(msg[0:6]) #Lisbon
print(msg[3:5]) #bo
print(msg[0:9]) #Lisbon is
print(msg[:9]) #Lisbon is
print(msg[10:14]) #in P
print(msg[10:]) #in Portugal
print(msg[:6]) #Lisbon
print(msg[6:]) # is in Portugal
print(msg[:6] + msg[6:]) #Lisbon is in Portugal
print(msg[:]) #Lisbon is in Portugal
#################################################
# Exercise 7
msg = "Lisbon is in Portugal"

print(msg[-21:-15]) #Lisbon
print(msg[-8:-5]) #Por
print(msg[-14:-12]) #is
print(msg[-8:]) #Portugal
print(msg[-11:-8]) #in
print(msg[-23:]) #Lisbon is in Portugal
print(msg[-5:-1]) #tuga
print(msg[7:]) #is in Portugal
print(msg[:]) #Lisbon is in Portugal