# -*- coding: utf-8 -*-
from random import randint
"""
@author: Francisco Colaço
"""
#Exercise 0
#1
print(3/2) #1.5
print(3//2) #1
print(3%2) #1
print(3**2) #9

#2
print((2+4)/2) #3
print((4+8+9)/3) #7
print((12+14/6+15)/3) #9.77

#3
def VolumeSphere(r):
    pi = 3.1415
    v = 4/3*pi*r*3
    return v

a = float(input("Please, enter radius: "))
print(f"The volume of the sphere is {VolumeSphere(a):.3f}") #62.83

#4
def EvenOrOdd(n):
    EoO=n%2
    if EoO == 0:
        print("Even")
    else:
        print("Odd")
EvenOrOdd(1) #Odd
EvenOrOdd(5) #Odd
EvenOrOdd(20) #Even
EvenOrOdd(60/7) #Odd

#######################################
#Exercise 1
random_rightNumber = randint(1,10)
guess = int(input("Guess the number. I'll give a tip, it's between 1 and 10\nYour Guess: "))
while random_rightNumber != guess:
    if random_rightNumber < guess:
        guess = int(input("Too High\nYour Guess: "))
    if random_rightNumber > guess:
        guess = int(input("Too Low\nYour Guess: "))
print("Correct, probably a lucky guess")
    