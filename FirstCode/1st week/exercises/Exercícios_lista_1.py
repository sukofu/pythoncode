# -*- coding: utf-8 -*-
"""
@author: Francisco Colaço
"""
# -1
# -1.a int
# -1.b float
# -1.c float
# -1.d string

#-2
word = input("write down a word\nWord: ")
print(word[::-1])

#-3
s = "abc"
#-3.a
s_len = len(s)
#-3.b
a,b,c = list(s)
s =a*3 + b*3 + c*3
print(s)

#-4
#-4.a
find_b = s.find("b")
find_ccc = s.find("ccc")
#-4.b
change_all_a = s.replace("a", "X")
change_1_a = s.replace("a", "X", 1)

#-5
s = "aaa bbb ccc"
#-5.1
s = s.replace("a","A")
s = s.replace("b","B")
s = s.replace("c","C")
print(s)
#-5.2
s = s.replace("B","b")
print(s)

#-6
a = 10 # 10
b = a # 10
c = 9 # 9
d = c # 9
c = c + 1 # 10

#-7
x=10
y=2
x, y = y, x

#-8
"""
No because if the number is higher than 100 it will contain both sentences, 
"The number is even and equal or higher than 100" and "The number is odd and equal or higher than 100"
The right solution should be:
"""
a = int ( input (" Enter a number: "))
if a % 2 == 0 and a < 100:
    print ("the number is even and smaller than 100")
elif a % 2 == 0 and a >= 100:
    print ("The number is even and equal or higher than 100")
elif a % 2 != 0 and a < 100:
    print ("The number is odd and smaller than 100")
elif  a % 2 != 0 and a >= 100:
    print ("The number is odd and equal or higher than 100")
