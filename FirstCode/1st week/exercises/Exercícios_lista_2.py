# -*- coding: utf-8 -*-
"""
@author: Francisco Colaço
"""
# Exercicio 1

num1 = int(input("Please input the first number\n>"))
operator = input("Please input a operator\n>")
num2 = int(input("Please input the second number\n>"))
if (operator == "-"):
    total = num1 - num2
    print(f"Your total:\n{num1}{operator}{num2}={total}")
elif (operator == "+"):
    total = num1 + num2
    print(f"Your total:\n{num1}{operator}{num2}={total}")
elif (operator == "/"):
    total = num1 / num2
    print(f"Your total:\n{num1}{operator}{num2}={total}")
elif (operator == "*"):
    total = num1 * num2
    print(f"Your total:\n{num1}{operator}{num2}={total}")
else:
    print("Invalid operator entered.")
    
# OR

opp = str(str(num1) + str(operator) + str(num2))
print("Operação:",opp)
print("Resultado:", eval(opp))

# Exercicio 2

while True:
    print("""1 - Pão Alentejano
2 - Bolo Lêvedo [dos Açores]
3 - Bolo do Caco [da Ilha da Madeira]
4 - Broa
5 – I want to leave """)
    selector = int(input(">"))
    if selector == 1:
        print("\nGood choice! Pão Alentejano is great. Would you like anything else?\n")
    elif selector == 2:
        print("\nGood choice! Bolo Lêvedo [dos Açores] is great. Would you like anything else?\n")
    elif selector == 3:
        print("\nGood choice! Bolo do Caco [da Ilha da Madeira] is great. Would you like anything else?\n")
    elif selector == 4:
        print("\nGood choice! Broa is great.Would you like anything else?\n")
    elif selector == 5:
        print("Please, come again!")
        break
    else: print("\nPlease, choose a number between 1 and 5.\n")

# Exercicio 3
# As my name starts with char "F" the output would be:
# x = 37 y = 1
# x = 18 y = 11
# x = 9 y = 110
# x = 4 y = 1101
# x = 2 y = 11010
# x = 1 y = 110100
# x = 0 y = 1101001
# x = 1 y = 110100
# x = 0 y = 11010
# x = 10 y = 1101
# x = 1 y = 110
# x = 10 y = 11
# x = 11 y = 1
# x = 1 y = 0

x = 5 + ord("F") # according with the unicode table, x would be 75
y = 0 # inicialize the variable as 0
while True: #runs forever until I say so 
    y = (x % 2) + 10 * y
    x = x // 2
    print("x =", x, "y =", y)
    if x == 0:
        break
while y != 0:
    x = y % 100
    y = y // 10
    print("x =", x, "y =", y)

# Exercicio 4

n = int(input("Please, input a positive integer\n>"))
Bspace = " "
if n <= 0:
    raise Exception("I clearly stated only positive integer")
for i in range(n):
    print(Bspace * i, i+1)
