# -*- coding: utf-8 -*-
"""
@author: Francisco Colaço
"""
import random

# Exercicio 1
# (5 > 4) and (3 == 5) OUTPUT: False
# not (5 > 4) OUTPUT: False
# (5 > 4) or (3 == 5) OUTPUT: True
# not ((5 > 4) or (3 == 5)) OUTPUT: False
# (True and True) and (True == False) OUTPUT: False
# (not False) or (not True) OUTPUT: True

# Exercicio 2
spam = 0
if spam == 10: # start block 1
    print("eggs")
    if spam > 5: # start block 2
        print("bacon")
    else: # start block 3 and end block 2 
        print("ham")
    print("spam") # end block 3
print("spam") # end block 1

# Exercicio 3
def range_of_numbers(n): 
    # generates n random integer numbers between -10 and 150
    lst=[]
    count_0_25 = count_26_50 = count_51_75 = count_76_100 = 0
    for i in range(n):
        random_number = random.randrange(-10, 150)
        lst.append(random_number)
    print(lst)
    for i in lst:
        if 0 <= i <= 25:
            count_0_25 += 1
        if 26 <= i <= 50:
            count_26_50 += 1
        if 51 <= i <= 75:
            count_51_75 += 1
        if 76 <= i <= 100:
            count_76_100 += 1
    print("[0, 25]: ", count_0_25)
    print("[26, 50]: ", count_26_50)
    print("[51, 75]: ", count_51_75)
    print("[76, 100]: ", count_76_100)
    
try:
    n = int(input("Input a number\n>"))
    if n <= 0: raise Exception("Next time, please input a positive integer value")
    range_of_numbers(n)
except:
    print("Next time, please input a positive integer value")
    
# Exercicio 4
sentence = """ start with The Portuguese: The Land and Its People (3) by Marion Kaplan (Penguin),
a one-volume introduction ranging from geography and history to wine and poetry,
and Portugal: A Companion History (4) by José H Saraiva (Carcanet Press), a
bestselling writer and popular broadcaster in his own country."""

# Method 1 -> removes any whitespace from the beginning
print("\nMethod 1:", sentence.strip()) 

# Method 2 -> returns the string in lower case
print("\nMethod 2:",sentence.lower())

# Method 3 -> returns the string in upper case 
print("\nMethod 3:",sentence.upper()) 

# Method 4 -> replaces a string with another string 
print("\nMethod 4:",sentence.replace("a", "h")) 

# Method 5 -> splits the string into substrings
print("\nMethod 5:",sentence.split(",")) 

# Method 6 -> returns True if all characters in the string are in the alphabet
print("\nMethod 6:",sentence.isalpha())  

# Method 7 -> returns True if all characters in the string are lower case         
print("\nMethod 7:",sentence.islower()) 

# Method 8 -> returns True if all characters in the string are upper case
print("\nMethod 8:",sentence.isupper()) 

# Method 9 -> searches the string for a specified value and returns the position of where it was found 
print("\nMethod 9:",sentence.index("from")) 

# Method 10 -> returns the count for the occurrence of the input substring
print("\nMethod 10:",sentence.count("a"))

# Exercicio 5
decimal_to_binary = int(input("Input a integer between 0 and 16:\n>"))
while decimal_to_binary > 16 or decimal_to_binary < 0:
    decimal_to_binary = int(input("Please input a integer between 0 and 16:\n>"))
print("\nYou choose: {}\nYour number in decimal: {}".format(decimal_to_binary, bin(decimal_to_binary)[2:]))